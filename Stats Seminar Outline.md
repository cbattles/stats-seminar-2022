# Gentuity Statistics Seminar, Fall 2022

## Section 1

### Analysis of data

#### Types of data

What about discrete and continuous data? <br>
Attribute and variable?<br>
Is Caroline versed in discrete statistics?  Because it hurts my brain.... <br>
Binomial is qualitaive (T/F)<br>

***Qualitative***<br>
Qualitative data is information that cannot be fundamentally represented as numbers <br>
Derived from interviews, questionaires, surveys, focus groups, etc.<br>
Examples: VOC, Clinical workflows, Geographies, Physicians, Failure types, etc.<br>
Qualitative data can sometimes be coded to transform it into quantitative-like data.  For example surveys on a Likert scale, relative importance of customer inputs, etc. <br>

***Quantitative***<br>
Quantitative data is numerical<br>
Not all numerical data is quantitative<br>
Typically generated from measurement, but not always<br>
Examples: Age, Income, Length, Weight, Number of failures, Dates, Brightness<br>

***Terminology***<br>
Coded - Transform Qualitiative data into "quantitative" data.  Assign numbers to the words, ideas or expressions to allow statistical treatments.<br>
Orderable - A hierarchy exists in the data.  Valid for Qualitiative and Quantitative data<br>

***Nominal***<br>
Coded but _unorderable_<br>
Examples: Language, Ethnicity, Sex, Hair Color, UDI<br>
How can you analyze it?:  Frequency analysis, percentages, proportions, typical values

***Ordinal***<br>
Coded and _orderable_<br>
Examples: Income, Age, Opinion (agree, disagree, mostly agree, etc)<br>
How can you analyze it?:  Same as Nominal data + summary statistics (mean, SE of mean, SD, median, range, etc.)<br>
There are arguments that with ordinal data, because the distance between some points in some data sets (e.g Likert scales) are not necessarily equivalent, then concepts like the mean value have no practical signficance. <br>

***Interval***<br>
Orderable, can add and subtract.  Can't multiply or divide. <br>
Negative values are possible.  No absolute point exists<br>
Examples: Temperature in Celsius, time (12:15 PM), dates (1947, 2012)<br>
How can you analyze it?:  Same as Ordinal data + ANOVA, regression, t-test, etc.

***Ratio***<br>
Orderable, can add, subtract, multiply and divide.<br>
Negative values are not possible<br>
Examples:  Age, SI units and derivatives (mks, cgs, ips), voltage, luminosity<br>
How can you analyze it?:  Same as interval data

#### Primer on Probability

Intuition vs reality
Notation

Prrobability distributions
discrete vs continuous
PDF vs CDF

#### Analysis options

Classical vs Baysian

EDA vs Classical

For classical analysis, the sequence is<br>
Problem => Data => Model => Analysis => Conclusions<br><br>
For EDA, the sequence is<br>
Problem => Data => Analysis => Model => Conclusions<br><br>
For Bayesian, the sequence is<br>
Problem => Data => Model => Prior Distribution => Analysis => Conclusions
<br><br>
https://xkcd.com/1132/


#### Sampling Data

How do we collect data?
Randomization!


# Random thoughts

Topics,
ANOVA vs ANOM<br>
Graphical methosd <br>
Regressions - linear, polynomial, exponential, logistic, ratio of polynomials, etc. <br>



Analyzng data allows us to make fact-backed decisions.  This means that the treatment of your data needs to be consistent and truely represent the facts.  It's easy to lie or mislead with statistics.  Our job is to ensure that does not happen.


At some point we need to talk about statistics that are robust against normality assumptions.

#### Modeling
Regressions<br>
Linear
Quadratic
Exponential
Logistic
Rational functions



